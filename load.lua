local worldLoaded = false
local DoorConfig = {
    [6]=180,
    [17]=180,
    [18]=180,
    [19]=180,
    [26]=180,
    [27]=180,
    [28]=180,
    [29]=180,
    [30]=180,
    [31]=180,
    [32]=180,
    [33]=180,
    [34]=180,
    [35]=0,
    [36]=270,
    [37]=270,
    [38]=270,
    [39]=270
}

local function Editor_CreateObject(objectID, x, y, z, rx, ry, rz, sx, sy, sz)
    local _object = CreateObject(objectID, x, y, z)
    if _object ~= false then
        if (rx ~= nil and sx ~= nil) then
            SetObjectRotation(_object, rx, ry, rz)
            SetObjectScale(_object, sx, sy, sz)
        end
        return _object
    end
    return false
end

local function Editor_CreateDoor(doorID, x, y, z, yaw)
    local _AddYaw = DoorConfig[tonumber(doorID)]
    if _AddYaw == nil then
        _AddYaw = 90
    end

    local _door = CreateDoor(doorID, x, y, z, yaw + _AddYaw)
    return _door
end

local function loadWorld()
    if worldLoaded then return end
    worldLoaded = true

    print('Server: Attempting to load world.')
    local _table = File_LoadJSONTable('world.json')
    if _table ~= nil then
        for _,v in pairs(_table) do
            local objectId
            if v['modelID'] ~= nil then
                objectId = Editor_CreateObject(v['modelID'], v['x'], v['y'], v['z'], v['rx'], v['ry'], v['rz'],
                        v['sx'], v['sy'], v['sz'])
                -- Special objects
                print("CreateObject("..v['modelID']..", "..v['x']..", "..v['y']..", "..v['z']..", "..v['rx']..", "
                        ..v['ry']..", "..v['rz']..", "..v['sx']..", "..v['sy']..", "..v['sz']..")")
            else
                objectId = Editor_CreateDoor(v['doorID'], v['x'], v['y'], v['z'], v['yaw'])
                print("CreateDoor("..v['doorID']..", "..v['x']..", "..v['y']..", "..v['z']..", "..v['yaw']..")")
            end
            CallEvent("WorldObjectLoaded", objectId, v)
        end

        print('Server: World loaded!')
    else
        print('Server: No world.json found in root server directory, one will be made next time the server saves.')
    end
end
AddEvent('OnPackageStart', loadWorld)
